import * as c from 'constants/actions'

const initialState = {
  isSidebarOpen: true
}

export default function ui(state = initialState, action) {
  switch (action.type) {
    case c.UI_INVERT_SIDEBAR_VISIBILITY:
      return { ...state, isSidebarOpen: !state.isSidebarOpen }

    case c.API_LOGOUT_USER_SUCCESS:
      return initialState
    default:
      return state
  }
}
