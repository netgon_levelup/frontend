import {
  Segment, Divider, Header, Image, Card, Button, Icon, Container
} from 'semantic-ui-react'
import moment from 'moment'

import { getAvatarUrl } from 'helpers/'
import { ARG_LIST, ARG_BLOCK } from 'constants/index'
import { SkillString } from 'components/commons'

import styles from './quests-item.module.scss'

class QuestsItem extends React.PureComponent {
  render() {
    const args = {}
    let content = null
    let mainContainerClassName = ''

    switch (this.props.type) {
      case ARG_LIST:
        args.header = { as: 'h4' }
        mainContainerClassName = 'full'
        content = (
          <Segment raised className={styles['list-item']}>
            { this.getContent(args) }
          </Segment>
        )
        break
      case ARG_BLOCK:
        args.header = { icon: true, textAlign: 'center' }
        mainContainerClassName = styles['container-block']
        content = (
          <Card>
            <div className={styles['wrapper-block']}>
              { this.getContent(args)}
            </div>
          </Card>
        )
        break
      default:
        return null
    }
    return (
      <div className={mainContainerClassName}>
        { content }
      </div>
    )
  }
  getContent(args = {}) {
    const { data } = this.props
    const { user } = data
    const { details } = user
    return (
      <Container fluid>
        <Header {...args.header}>
          { details.avatar !== null && (
            <Image
              centered
              size='large'
              shape='circular'
              className='pointer'
              src={getAvatarUrl(details.avatar)}
            />
          )}
          <Header.Content>
            <span className={styles.name}>{ user.username }</span>
            {/* <span>{ data.team.length > 0 && ` из ${data.team}` }</span> */}
            <Header.Subheader className='top-level hight'>
              {`Требуемый уровень не ниже ${data.level}`}
            </Header.Subheader>
          </Header.Content>
        </Header>
        <Divider />
        <div>
          <p className={styles.date}>{ moment(data.created).format('DD MMMM YYYY').toUpperCase() }</p>
          <h2 className={styles.title}>{ data.name }</h2>
          <p className={styles.desc}>{ data.description }</p>
        </div>
        <div className={styles['content-bottom']}>
          <Button onClick={this.props.onSelect} className={styles.more}>
            Подробнее
            <Icon name='long arrow right' />
          </Button>
          <span className={styles.level}>
            Опыт: undef exp.
          </span>
        </div>
        <Divider />
        <SkillString items={data.skills} />
      </Container>
    )
  }
}


QuestsItem.defaultProps = {
  data: {
    team: '',
    level: 0
  },
  type: ARG_LIST
}
QuestsItem.propTypes = {
  data: PropTypes.shape({
    user: PropTypes.shape({
      username: PropTypes.string
    }),
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    created: PropTypes.string.isRequired,
    skills: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired
    }))
  }).isRequired,

  type: PropTypes.oneOf([ARG_LIST, ARG_BLOCK]).isRequired,
  onSelect: PropTypes.func.isRequired
}

export default QuestsItem
