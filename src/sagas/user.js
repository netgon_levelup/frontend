import { call, put, takeEvery, select } from 'redux-saga/effects'

import { isUnprotected, redirect } from 'helpers/routing'
import * as token from 'helpers/token'
import * as c from 'constants/actions'
import * as a from 'actions/user'
import * as r from 'constants/routes'
import * as api from './api/user'
import * as apiStorage from './api/storage'
import * as selectorRouting from './selectors/routing'

function* handleRegister({ payload }) {
  try {
    const data = { ...payload }
    if (data.details.avatar !== null) {
      const form = new FormData()
      form.append('file', data.details.avatar, data.details.avatar.name)
      const avatar = yield call(apiStorage.uploadAvatar(form))

      data.details.avatar = avatar.result.files.file[0].name
    }
    const result = yield call(api.register(data))
    yield put(a.successRegister(result))

    redirect(r.LOGIN)
  } catch ({ message }) {
    yield put(a.errorRegister(message))
  }
}
function* handleLogin(action) {
  const { email, password, remember, byToken } = action.payload
  const pathname = yield select(selectorRouting.getPathname)
  try {
    let userId = 0
    if (byToken === true) {
      const tkn = token.get()
      if (tkn === null) throw new Error('Storage does not contain a token')
      const result = yield call(api.loginByToken())
      userId = result.userId
    } else {
      const result = yield call(api.login({ email, password }))
      userId = result.userId
      token.set(result.id, remember)
    }
    const resultGet = yield call(api.get({ id: userId }))

    yield put(a.successLogin(resultGet))
    if (isUnprotected(pathname) === true) redirect(r.QUESTS)
  } catch (error) {
    yield put(a.errorLogin(error.message))

    if (pathname !== r.LOGIN) redirect(r.LOGIN)
  }
}
function* handleLogout() {
  try {
    yield call(api.logout())

    yield put(a.successLogout())

    token.clear()
    redirect(r.LOGIN)
  } catch (error) {
    yield put(a.errorLogout(error.message))
  }
}

export default function* watcherUser() {
  yield takeEvery(c.API_REGISTER_USER_REQUEST, handleRegister)
  yield takeEvery(c.API_LOGIN_USER_REQUEST, handleLogin)
  yield takeEvery(c.API_LOGOUT_USER_REQUEST, handleLogout)
}
