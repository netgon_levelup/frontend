import { combineReducers } from 'redux'

import ui from './ui'
import user from './user'
import skill from './skill'
import project from './project'
import proposal from './proposal'
import category from './category'

export default combineReducers({
  ui,
  user,
  skill,
  project,
  proposal,
  category
})
