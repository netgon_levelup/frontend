import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  Segment, Header, Icon, Divider, Form, Button
} from 'semantic-ui-react'
import _ from 'lodash'

import { requestAdd } from 'actions/project'
import { requestGet as getSkill } from 'actions/skill'
import { requestGet as getCategory } from 'actions/category'
import { FilesAdd } from 'components/commons'
import { handleInput } from 'helpers/'

import styles from './quests-new.module.scss'

function getOption(key, value, text) {
  return { key, value, text }
}
function getOptions(arr = []) {
  return arr.map(({ id, name }) => getOption(id, id, name))
}
const levels = _.times(80).map(item => getOption(item, item, item))

class QuestsNew extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleName = handleInput.bind(this)('name')
    this.handleCategory = (e, { value }) => this.setState({ category: value })
    this.handleCategorySearch = (e, filter) => this.updCatState({ search: filter }, () => this.props.getCategory({ filter }))
    this.handleCategoryAdd = (e, { value }) => this.updCatState({ value })
    this.handleSkillSearch = (e, filter) => this.updTechState({ search: filter }, () => this.props.getSkill({ filter }))
    this.handleSkillAdd = (e, { value }) => this.updTechState({ value })
    this.handleDescription = handleInput.bind(this)('description')
    this.handleLevel = (e, { value }) => this.setState({ level: value })
    this.handleFilesAdd = ::this.handleFilesAdd
    this.handleFilesRemove = ::this.handleFilesRemove
    this.handleSubmit = ::this.handleSubmit

    this.state = {
      name: 'test',
      description: 'lorem ipsum and blah-blah-blah',
      level: '',
      files: [],
      category: {
        search: '',
        options: [],
        value: ''
      },
      skill: {
        search: '',
        options: [],
        value: []
      },
      errors: {
        name: false,
        description: false
      }
    }
  }
  componentDidMount() {
    this.props.getSkill()
    this.props.getCategory()
  }
  componentWillReceiveProps(nextProps) {
    const oldSkill = this.state.skill.options
    const newSkill = getOptions(nextProps.skill.data)
    if (JSON.stringify(oldSkill) !== JSON.stringify(newSkill)) {
      this.updTechState({ options: newSkill })
    }
    const oldCat = this.state.category.options
    const newCat = getOptions(nextProps.category.data)
    if (JSON.stringify(oldCat) !== JSON.stringify(newCat)) {
      this.updCatState({ options: newCat })
    }
  }

  render() {
    /* eslint max-len: 0 */
    return (
      <div className={`${styles.container} content-offset`}>
        <div className='section-title'>
          <h2>Добавить квест</h2>
        </div>

        <Segment raised className='panel panel-quest-new'>
          <Header as='h4' className={styles['panel-header']}>
            <Icon name='icon-Bulb' className='icon-gap' color='yellow' size='massive' />
            <Header.Content>
              <p className={styles.title}>{ 'Совета по добавлению квестов:' }</p>
              <Header.Subheader className={styles.subheader}>
                <p>Ваш проект должен быть таким-то и таким-то и должен отвечать тем или иным требованиям</p>
                <p>Опишите свой квест максимально просто и доступно. Это позволин вам получить отклики от профессиональных пользователей</p>
              </Header.Subheader>
            </Header.Content>
          </Header>
          <Divider />
          <Form className={styles.form}>
            <Form.Input
              width={8}
              label='Название квеста'
              error={this.state.errors.name}
              value={this.state.name}
              onChange={this.handleName}
            />
            <Form.Dropdown
              search
              selection
              className='form-select'
              width={8}
              label='Категория'
              value={this.state.category.value}
              options={this.state.category.options}
              loading={this.props.category.fetching}
              disabled={this.props.category.fetching}
              onChange={this.handleCategoryAdd}
              onSearchChange={this.handleCategorySearch}
            />
            <Form.Dropdown
              search
              multiple
              selection
              className='form-select'
              width={8}
              label='Стек технологий'
              value={this.state.skill.value}
              options={this.state.skill.options}
              loading={this.props.skill.fetching}
              disabled={this.props.skill.fetching}
              onChange={this.handleSkillAdd}
              onSearchChange={this.handleSkillSearch}
            />
            <Form.TextArea
              className={styles['field-desc']}
              label='Описание квеста'
              error={this.state.errors.description}
              value={this.state.description}
              onChange={this.handleDescription}
            />
            <Form.Dropdown
              label='Необходимый уровень исполнителя'
              width={8}
              selection
              className='form-select'
              options={levels}
              value={this.state.level}
              onChange={this.handleLevel}
            />
            <Form.Field width={8} className={styles.attachments}>
              <label>Прикрепить файлы</label>
              <FilesAdd
                onDrop={this.handleFilesAdd}
                accept='image/jpeg, image/png'
                files={this.state.files}
                onRemoveTag={this.handleFilesRemove}
              />
            </Form.Field>
            <Button
              className='submit'
              color='yellow'
              onClick={this.handleSubmit}
              loading={this.props.project.fetching}
            >
              Отправить
            </Button>
          </Form>
        </Segment>
      </div>
    )
  }

  updTechState(params, callback = null) {
    if (callback === null) {
      this.setState(this.getSubFieldState('skill', params))
    } else {
      this.setState(this.getSubFieldState('skill', params), callback)
    }
  }
  updCatState(params) {
    this.setState(this.getSubFieldState('category', params))
  }
  getSubFieldState(field, params) {
    return {
      [field]: {
        ...this.state[field],
        ...params
      }
    }
  }

  handleFilesAdd(files) {
    const file = files[0]
    const exist = !!this.state.files.find(item => item.name === file.name)
    if (exist === false) this.setState({ files: this.state.files.concat([file]) })
  }
  handleFilesRemove(i) {
    const files = this.state.files.filter((el, j) => j !== i)
    return () => this.setState({ files })
  }
  handleSubmit() {
    const { name, description, category, skill, level, files } = this.state
    const data = {
      name,
      description,
      level,
      category: category.value,
      skills: skill.value,
      files
    }
    console.log(data)
    this.props.addProject(data)
  }
}

QuestsNew.propTypes = {
  project: PropTypes.shape({
    fetching: PropTypes.bool.isRequired
  }).isRequired,
  skill: PropTypes.shape({
    fetching: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired
  }).isRequired,
  category: PropTypes.shape({
    fetching: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired
  }).isRequired,

  addProject: PropTypes.func.isRequired,
  getSkill: PropTypes.func.isRequired,
  getCategory: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    project: {
      fetching: state.rootReducer.project.fetching
    },
    skill: {
      fetching: state.rootReducer.skill.fetching,
      data: state.rootReducer.skill.data
    },
    category: {
      fetching: state.rootReducer.category.fetching,
      data: state.rootReducer.category.data
    }
  }
}
function mapDispatchToProps(dispatch) {
  return {
    addProject: bindActionCreators(requestAdd, dispatch),
    getSkill: bindActionCreators(getSkill, dispatch),
    getCategory: bindActionCreators(getCategory, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(QuestsNew)
