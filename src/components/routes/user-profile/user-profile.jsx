import { connect } from 'react-redux'
import { Segment, Grid, Image, Icon, Progress, Divider } from 'semantic-ui-react'

import { getAvatarUrl } from 'helpers/'

import styles from './user-profile.module.scss'

class UserProfile extends React.PureComponent {
  state = {}
  render() {
    const { user } = this.props
    const { details } = user
    return (
      <div className='content-offset'>
        <div className='section-title'>
          <h2>
            Профиль: { details.firstName || '' } { details.middleName || '' } { details.lastName || '' }
          </h2>
        </div>
        <Segment raised className='panel panel-profile'>
          <Grid className={styles['panel-content']}>
            <Grid.Column>
              <Image
                centered
                size='medium'
                shape='circular'
                className={styles.avatar}
                src={getAvatarUrl(details.avatar)}
              />
            </Grid.Column>
            <Grid.Column width={16}>
              <Grid.Row>
                <Grid className={styles.top} verticalAlign='middle'>
                  <Grid.Column>
                    <h4 className={styles.name}>{ user.username }</h4>
                  </Grid.Column>
                  <Grid.Column textAlign='right'>
                    <Icon.Group size='big' className={styles.edit}>
                      <Icon name='circle outline' className='icon-with-circle' />
                      <Icon name='edit' className='icon-gap icon-Pencil' size='tiny' />
                    </Icon.Group>
                  </Grid.Column>
                </Grid>
              </Grid.Row>
              <Grid.Row className={styles.skills}>
                <Grid>
                  <Grid.Column className={styles['progress-row']} width={16}>
                    <Progress
                      percent={60}
                      indicating
                      inverted
                      className='striped'
                      color='purple'
                    />
                    <div className={styles['skills-details']}>
                      <p className={styles.level}>LVL { details.level }</p>
                      <p className={styles.detail}>{ details.experience } / undef</p>
                    </div>
                  </Grid.Column>
                </Grid>
              </Grid.Row>
              <div className={styles.list}>
                <p>
                  <label>Паспорт: </label>
                  <span>12345</span>
                </p>
                <p>
                  <label>Пол: </label>
                  <span>{ details.gender === 1 ? 'Мужской' : 'Женский' }</span>
                </p>
                <p>
                  <label>Email: </label>
                  <a href='#'>{ user.email }</a>
                </p>
                <p>
                  <label>Телефон: </label>
                  <span>{ details.phone }, </span>
                  <span>{ details.phone } </span>
                  <a href='#' className={styles['add-number']}>
                    <Icon name='plus' />
                  </a>
                </p>
              </div>

              <Divider />
              <div className={styles.character}>
                <h5>Кратко о персонаже:</h5>
                <p>{ details.description }</p>
              </div>
            </Grid.Column>
          </Grid>
        </Segment>
      </div>
    )
  }
}

UserProfile.propTypes = {
  user: PropTypes.object.isRequired
}

export default connect(
  state => ({
    user: state.rootReducer.user.data
  })
)(UserProfile)
