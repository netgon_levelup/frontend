import ReactDropzone from 'react-dropzone'
import classNames from 'classnames'

import styles from './dropzone.module.scss'

const Dropzone = props => (
  <ReactDropzone
    id='drop-zone'
    className={classNames({
      [styles.container]: true,
      [props.className]: props.className !== undefined
    })}
    onDrop={props.onDrop}
    accept={props.accept}
  >
    <div className={styles.child}>
      {props.children}
    </div>
  </ReactDropzone>
)

Dropzone.defaultProps = {
  accept: '',
  className: '',
  onDrop: (...args) => console.log(args),
  children: null
}

Dropzone.propTypes = {
  accept: PropTypes.string,
  className: PropTypes.string,
  onDrop: PropTypes.func,
  children: PropTypes.node
}

export default Dropzone
