import * as a from 'constants/actions'

export function invertSidebarVisibility() {
  return {
    type: a.UI_INVERT_SIDEBAR_VISIBILITY
  }
}
