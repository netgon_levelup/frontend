import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Segment, Header, Button, Checkbox, Form } from 'semantic-ui-react'

import { requestLogin } from 'actions/user'

import styles from './login.module.scss'

class Login extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleEmail = e => this.setState({ email: e.target.value })
    this.handlePassword = e => this.setState({ password: e.target.value })
    this.handleSubmit = ::this.handleSubmit

    this.state = {
      email: props.email || '',
      password: '',
      remember: true
    }
  }

  render() {
    return (
      <Segment className='form_container'>
        <Header as='h2' icon textAlign='center'>
          <Header.Content>
            Вход
          </Header.Content>
        </Header>
        <br />
        <Form>
          <Form.Input label='Email' value={this.state.email} onChange={this.handleEmail} />
          <Form.Input label='Пароль' type='password' value={this.state.password} onChange={this.handlePassword} />
        </Form>
        <div className={styles['row-rf']}>
          <Checkbox
            label='Запомнить меня'
            className={styles.remember}
            checked={this.state.remember}
            onClick={() => this.setState({ remember: !this.state.remember })}
          />
          <a
            className={styles.forgot}
          >
            Забыли пароль?
          </a>
        </div>
        <Button
          fluid
          color='yellow'
          className='submit'
          loading={this.props.fetching}
          onClick={this.handleSubmit}
        >
          ВОЙТИ
        </Button>
      </Segment>
    )
  }

  handleSubmit() {
    const { email, password, remember } = this.state

    const data = {
      email, password, remember
    }
    this.props.requestLogin(data)
  }
}

Login.defaultProps = {
  email: ''
}
Login.propTypes = {
  email: PropTypes.string,
  fetching: PropTypes.bool.isRequired,

  requestLogin: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const user = state.rootReducer.user
  return {
    email: user.data.email,
    fetching: user.fetching
  }
}
function mapDispatchToProps(dispatch) {
  return {
    requestLogin: bindActionCreators(requestLogin, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)

