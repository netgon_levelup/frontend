import { Menu } from 'semantic-ui-react'
import _ from 'lodash'

import styles from './pagination.module.scss'

const Pagination = props => (
  <div className={styles.container}>
    <Menu pagination>
      <Menu.Item
        icon='long arrow left'
        content='предыдущие'
        disabled={props.current === 1}
        onClick={props.onDecrement}
      />
      { _.times(props.total).map((item, i) => {
        const curr = i + 1
        return (
          <Menu.Item
            key={item}
            name={String(curr)}
            active={props.current === curr}
            onClick={() => props.onChange(curr)}
          />
        )
      })}
      <Menu.Item
        icon='long arrow right'
        content='следующиe'
        disabled={props.current === props.total}
        onClick={props.onIncrement}
      />
    </Menu>
  </div>
)

Pagination.propTypes = {
  current: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  total: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,

  onDecrement: PropTypes.func.isRequired,
  onIncrement: PropTypes.func.isRequired
}

export default Pagination
