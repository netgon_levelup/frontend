import * as c from 'constants/actions'

const initialState = {
  data: [],
  fetching: false,
  error: false
}

export default function project(state = initialState, action) {
  switch (action.type) {
    case c.API_GET_CATEGORY_REQUEST:
      return { ...state, fetching: true }
    case c.API_GET_CATEGORY_SUCCESS:
      return { ...state, fetching: false, data: action.payload }
    case c.API_GET_CATEGORY_ERROR:
      return { ...state, fetching: false, error: true }

    case c.API_LOGOUT_USER_SUCCESS:
      return initialState
    default:
      return state
  }
}
