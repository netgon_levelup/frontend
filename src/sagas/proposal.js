import {
  call,
  put,
  select,
  takeEvery
} from 'redux-saga/effects'

import * as c from 'constants/actions'
import * as action from 'actions/proposal'
import * as api from './api/proposal'
import {
  getId as getUserId
} from './selectors/user'
import {
  getId as getProjectId
} from './selectors/project'

function* handleAdd({ payload }) {
  try {
    const params = {
      text: payload,
      projectId: yield select(getProjectId),
      userId: yield select(getUserId)
    }
    const result = yield call(api.add(params))

    if ('error' in result) {
      if (result.error.constraint === 'project_user_idx') {
        yield put(action.errorAdd({ uniqueness: true }))
      }
    } else {
      const proposal = yield call(api.getById(result.id))
      yield put(action.successAdd(proposal))
    }
  } catch (error) {
    yield put(action.errorAdd(true))
  }
}

function* handleGetList({ payload }) {
  try {
    const projectId = payload
    const result = yield call(api.getByProjectId(projectId))
    console.log(projectId, result)
    yield put(action.successGetList(result))
  } catch (error) {
    yield put(action.errorGetList(true))
  }
}

export default function* watcherUser() {
  yield takeEvery(c.API_ADD_PROPOSAL_REQUEST, handleAdd)
  yield takeEvery(c.API_GET_PROPOSALS_REQUEST, handleGetList)
}
