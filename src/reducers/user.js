import * as c from 'constants/actions'

const initialState = {
  data: {},
  authenticated: false,
  fetching: false,
  error: false
}

export default function (state = initialState, action) {
  switch (action.type) {
    case c.API_REGISTER_USER_REQUEST:
      return { ...state, fetching: true }
    case c.API_REGISTER_USER_SUCCESS:
      return { ...state, fetching: false, data: action.payload }
    case c.API_REGISTER_USER_ERROR:
      return { ...state, fetching: false, error: action.payload }

    case c.API_LOGIN_USER_REQUEST:
      return { ...state, fetching: true }
    case c.API_LOGIN_USER_SUCCESS:
      return { ...state, fetching: false, data: action.payload, authenticated: true }
    case c.API_LOGIN_USER_ERROR:
      return { ...state, fetching: false, error: action.payload }

    case c.API_LOGOUT_USER_SUCCESS:
      return initialState

    default:
      return state
  }
}
