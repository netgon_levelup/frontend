import * as c from 'constants/actions'

const initialState = {
  data: {},
  list: [],
  count: 0,
  fetching: false,
  error: false
}

export default function project(state = initialState, action) {
  switch (action.type) {
    case c.API_GET_PROJECTS_REQUEST:
    case c.API_ADD_PROJECT_REQUEST:
      return { ...state, fetching: true }

    case c.API_ADD_PROJECT_SUCCESS:
      return { ...state, fetching: false }
    case c.API_GET_PROJECT_SUCCESS:
      return { ...state, fetching: false, data: action.payload }
    case c.API_GET_PROJECTS_SUCCESS:
      return { ...state, fetching: false, ...action.payload }

    case c.API_GET_PROJECTS_ERROR:
    case c.API_ADD_PROJECT_ERROR:
      return { ...state, fetching: false, error: action.payload }

    case c.SELECT_PROJECT_DETAILS:
      return { ...state, data: action.payload }

    case c.API_LOGOUT_USER_SUCCESS:
      return initialState

    default:
      return state
  }
}
