import axios from 'helpers/axios'
// import { getToken } from './'

const BASE = 'proposals'

const defaultInclude = {
  relation: 'user',
  scope: {
    include: 'details'
  }
}

export const add = params => async () => {
  const result = await axios.post(`${BASE}`, params)

  return result.data
}
export const getById = id => async () => {
  const filter = {
    include: defaultInclude
  }
  const result = await axios.get(`${BASE}/${id}/?filter=${JSON.stringify(filter)}`)

  return result.data
}

export const getByProjectId = id => async () => {
  const filter = {
    where: {
      projectId: id
    },
    include: defaultInclude
  }
  const result = await axios.get(`${BASE}/?filter=${JSON.stringify(filter)}`)

  return result.data
}
