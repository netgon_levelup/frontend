import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Router, Route, Redirect, IndexRedirect } from 'react-router'

import { requestLogin } from 'actions/user'
import { App, Lobby, Dashboard } from 'components/containers'
import * as rc from 'components/routes/'

import * as r from 'constants/routes'

class Routes extends React.Component {
  constructor(props) {
    super(props)

    this.initialAuth = ::this.initialAuth
    this.checkAuth = ::this.checkAuth
  }
  shouldComponentUpdate() {
    return false
  }
  render() {
    return (
      <Router history={this.props.history}>
        <Route path={r.INDEX} component={App}>
          <IndexRedirect to={r.QUESTS} />
          <Route component={Dashboard} onEnter={this.initialAuth}>
            <Route path={r.QUESTS_NEW} component={rc.QuestsNew} />
            <Route path={r.QUESTS} component={rc.Quests} />
            <Route path={`${r.QUESTS}/:id`} component={rc.QuestsDetails} />

            <Route path={r.USER_PROFILE} component={rc.UserProfile} />
            <Route path={r.USER_SKILLS} component={rc.UserSkills} />
            <Route path={r.USER_LEVEL} component={rc.UserLevel} />
            <Route path={r.USER_EXPERIENCE} component={rc.UserExperience} />
            <Route path={r.USER_GOLD} component={rc.UserGold} />
          </Route>
          <Route component={Lobby} onEnter={this.checkAuth}>
            <Route path={r.LOGIN} component={rc.Login} />
            <Route path={r.REGISTRATION} component={rc.Registration} />
          </Route>
          <Redirect from={r.ANY} to={r.INDEX} />
        </Route>
      </Router>
    )
  }
  initialAuth() {
    if (this.props.authenticated === false) {
      this.props.requestLogin({ byToken: true })
    }
  }
  checkAuth(nextState, replace) {
    if (this.props.authenticated === true) {
      replace(r.QUESTS)
    }
  }
}

Routes.propTypes = {
  authenticated: PropTypes.bool.isRequired,

  history: PropTypes.object.isRequired,
  requestLogin: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    authenticated: state.rootReducer.user.authenticated
  }
}
function mapDispatchToProps(dispatch) {
  return {
    requestLogin: bindActionCreators(requestLogin, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Routes)
