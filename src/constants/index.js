export const ENV_PRODUCTION = 'production'

export const TOKEN = 'auth-token'

export const NO_IMAGE_SELECTED = '/assets/noImageSelected.png'

export const LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

export const STORAGE_USER_AVATAR = 'user-avatar'
export const STORAGE_PROJECT_FILES = 'project-files'

export const ARG_LIST = 'list'
export const ARG_BLOCK = 'block'
