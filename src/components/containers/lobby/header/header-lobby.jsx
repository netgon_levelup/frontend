import { connect } from 'react-redux'
import { Button, Header, Image, Grid } from 'semantic-ui-react'
import classNames from 'classnames'

import { Header as HeaderContainer } from 'components/containers'
import { TabsHeader } from 'components/commons'
import { LOGIN, REGISTRATION } from 'constants/routes'
import { redirect } from 'helpers/routing'

import styles from './header-lobby.module.scss'

class LobbyHeader extends React.PureComponent {
  constructor(props) {
    super(props)

    this.redirectLogin = () => redirect(LOGIN)
    this.redirectRegistration = () => redirect(REGISTRATION)
  }
  render() {
    return (
      <HeaderContainer>
        <Grid className={styles.row}>
          <Grid.Column className={styles['column-left']}>
            <Header as='a' className={styles['logo-title']}>
              <Header.Content className={styles['logo-image']}>
                <Image name='plug' src='/assets/header-logo.png' />
              </Header.Content>
            </Header>
          </Grid.Column>
          <Grid.Column className={styles['column-center']}>
            <TabsHeader />
          </Grid.Column>
          <Grid.Column textAlign='right' className={styles['column-right']}>
            <Button.Group className={styles['nav-right-buttons']}>
              <Button
                basic
                color='yellow'
                className={classNames({
                  [styles.account]: true,
                  [styles.active]: this.props.location !== REGISTRATION
                })}
                onClick={this.redirectRegistration}
              >
                Регистрация
              </Button>
              <Button
                basic
                color='yellow'
                className={classNames({
                  [styles.account]: true,
                  [styles.active]: this.props.location !== LOGIN
                })}
                onClick={this.redirectLogin}
              >
                Вход
              </Button>
            </Button.Group>
          </Grid.Column>
        </Grid>
      </HeaderContainer>
    )
  }
}

LobbyHeader.propTypes = {
  location: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    location: state.routing.locationBeforeTransitions.pathname
  }
}
function mapDispatchToProps() {
  return { }
}

export default connect(mapStateToProps, mapDispatchToProps)(LobbyHeader)
