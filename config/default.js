module.exports = {
  port: parseInt(process.env.APP_PORT, 10) || 8080,
  backend: {
    url: 'http://localhost:3030/api'
  }
}
