import { connect } from 'react-redux'
import { Grid, Icon, Step, Accordion, Divider, Header } from 'semantic-ui-react'

import { ProfileHeader } from 'components/commons'

import styles from './user-skills.module.scss'

const ACC_DESIGNER = 'designer'
const ACC_DEVELOPER = 'developer'
const ACC_COPYWRITER = 'copywriter'

class UserProfile extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleUncovered = ::this.handleUncovered

    this.state = {
      uncovered: [ACC_DESIGNER]
    }
  }
  render() {
    const { user } = this.props
    const { details } = user
    return (
      <div className='content-offset'>
        <div className='section-title'>
          <h2>
            Профиль
          </h2>
        </div>
        <ProfileHeader
          avatar={details.avatar}
          username={user.username}
          level={details.level}
          experience={details.experience}
        />
        <div className={styles.accordions}>
          <Accordion
            styled
            fluid
            className={`${styles.accordion} panel`}
          >
            <Accordion.Title onClick={this.handleUncovered(ACC_DESIGNER)}>
              { this.getAccordionTitle(ACC_DESIGNER, 'Ветка Дизайнера', 'Ваш уровень дизайнера: ', '2') }
            </Accordion.Title>
            <Accordion.Content
              active={this.state.uncovered.includes(ACC_DESIGNER)}
            >
              <Divider />
              <div className={styles.steps}>
                { this.getAccordionStep('LVL 1', 'User', ['Начинающий', 'дизайнер'], true) }
                { this.getAccordionStep('LVL 2', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 3', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 4', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 5', 'User', ['Начинающий', 'дизайнер']) }
              </div>
            </Accordion.Content>
          </Accordion>
          <Accordion
            styled
            fluid
            className={`${styles.accordion} panel`}
          >
            <Accordion.Title onClick={this.handleUncovered(ACC_DEVELOPER)}>
              { this.getAccordionTitle(ACC_DEVELOPER, 'Ветка Программиста', 'Ваш уровень программиста: ', '2') }
            </Accordion.Title>
            <Accordion.Content
              active={this.state.uncovered.includes(ACC_DEVELOPER)}
            >
              <Divider />
              <div className={styles.steps}>
                { this.getAccordionStep('LVL 1', 'User', ['Начинающий', 'дизайнер'], true) }
                { this.getAccordionStep('LVL 2', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 3', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 4', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 5', 'User', ['Начинающий', 'дизайнер']) }
              </div>
            </Accordion.Content>
          </Accordion>
          <Accordion
            styled
            fluid
            className={`${styles.accordion} panel`}
          >
            <Accordion.Title onClick={this.handleUncovered(ACC_COPYWRITER)}>
              { this.getAccordionTitle(ACC_COPYWRITER, 'Ветка Копирайтера', 'Ваш уровень копирайтера: ', '2') }
            </Accordion.Title>
            <Accordion.Content active={this.state.uncovered.includes(ACC_COPYWRITER)} >
              <Divider />
              <div className={styles.steps}>
                { this.getAccordionStep('LVL 1', 'User', ['Начинающий', 'дизайнер'], true) }
                { this.getAccordionStep('LVL 2', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 3', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 4', 'User', ['Начинающий', 'дизайнер']) }
                { this.getAccordionStep('LVL 5', 'User', ['Начинающий', 'дизайнер']) }
              </div>
            </Accordion.Content>
          </Accordion>
        </div>
      </div>
    )
  }
  getAccordionTitle(label, branch, level, exp) {
    return (
      <Grid>
        <Grid.Column floated='left' width={5}>
          <Header
            as='h4'
            icon={this.state.uncovered.includes(label) === true ? 'minus' : 'plus'}
            content={branch}
          />
        </Grid.Column>
        <Grid.Column className={styles.level} floated='right' width={4}>
          <span>{ level }</span>
          <span className={styles.exp}>{ exp }</span>
        </Grid.Column>
      </Grid>
    )
  }

  getAccordionStep(title, icon, desc, state) {
    // const stepIcons = [{
    //   icon: 'CompassTool'
    // }, {
    //   icon: 'Delete'
    // }, {
    //   icon: 'Pen'
    // }, {
    //   icon: 'Pen'
    // }, {
    //   icon: 'Pen'
    // }]
    return (
      <Step active={state} className={styles.step}>
        <h4>{ title }</h4>
        <div className={styles.stepIcon}>
          <Icon name='user' className={`icon-gap icon-User`} />
          <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 112 127.5'><path className='st0' d='M46 3.8C52.5.1 59.2.1 66 3.8l33 20c9 6 12 10 12 17v46c0 8-5 13-13 18l-32 19c-6.8 3.6-13.5 3.6-20 0l-32-19c-9-5-13-10-13-18v-46c0-7 4-12 12-17l33-20z' /></svg>
        </div>
        <div className={styles.stepDesc}>
          <p>{ desc[0] }</p>
          <p>{ desc[1] }</p>
        </div>
      </Step>
    )
  }

  handleUncovered = label => () => {
    const { uncovered } = this.state
    if (uncovered.includes(label) === true) {
      this.setState({ uncovered: uncovered.filter(item => item !== label) })
    } else {
      this.setState({ uncovered: uncovered.concat([label]) })
    }
  }
}

UserProfile.propTypes = {
  user: PropTypes.object.isRequired
}


export default connect(
  state => ({
    user: state.rootReducer.user.data
  })
)(UserProfile)
