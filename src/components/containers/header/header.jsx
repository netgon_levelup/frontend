import { Segment } from 'semantic-ui-react'

import styles from './header.module.scss'

const Header = props => (
  <Segment className={styles.container}>
    { props.children }
  </Segment>
)

Header.propTypes = {
  children: PropTypes.node.isRequired
}

export default Header
