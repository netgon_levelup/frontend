import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  Button, Icon
} from 'semantic-ui-react'

import { invertSidebarVisibility } from 'actions/ui'

import styles from './sidebar.module.scss'

class Sidebar extends React.PureComponent {
  render() {
    return (
      <div className={styles.container}>
        { this.props.isSidebarOpen === true && this.props.children }
        <Button
          basic
          onClick={this.props.invertSidebarVisibility}
          className={styles['btn-visibility']}
        >
          <Icon
            name={`chevron ${this.props.isSidebarOpen ? 'left' : 'right'}`}
            size='big'
            className={styles.chevron}
          />
        </Button>
      </div>
    )
  }
}

Sidebar.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,

  invertSidebarVisibility: PropTypes.func.isRequired,

  children: PropTypes.node.isRequired
}

function mapStateToProps(state) {
  return {
    isSidebarOpen: state.rootReducer.ui.isSidebarOpen
  }
}
function mapDispatchToProps(dispatch) {
  return {
    invertSidebarVisibility: bindActionCreators(invertSidebarVisibility, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Sidebar)
