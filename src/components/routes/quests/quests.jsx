import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Input, Button, Dropdown, Loader, Container } from 'semantic-ui-react'

import Masonry from 'react-masonry-component'

import {
  requestGetList as getProjects,
  selectDetails as selectProjectDetails
} from 'actions/project'
import { QuestsItem, Pagination, PaginationAdd } from 'components/commons'
// import { ARG_LIST, ARG_BLOCK } from 'constants/index'
import { ARG_LIST, ARG_BLOCK } from 'constants/index'

import styles from './quests.module.scss'

const optionsTime = [{ key: 'all', value: 'all', text: 'За все время' }]
const optionsDifficulty = [{ key: 'hard', value: 'hard', text: 'Сначала сложные' }]
const optionsTarget = [{ key: 'team', value: 'team', text: 'Сначала для команды' }]

const optionsMasonry = {
  transitionDuration: 0
}

class Quests extends React.PureComponent {
  static propTypes = {
    project: PropTypes.shape({
      list: PropTypes.array,
      fetching: PropTypes.bool
    }).isRequired,

    getProjects: PropTypes.func.isRequired,
    selectProjectDetails: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.handleSelectItem = item => () => this.props.selectProjectDetails(item)
    this.decCurrPage = () => this.setState({ currPage: this.state.currPage - 1 })
    this.incCurrPage = () => this.setState({ currPage: this.state.currPage + 1 })
    this.changePage = i => this.setState({ currPage: i })
    this.changeDisplayType = () => this.setState({
      display: this.state.display === ARG_LIST ? ARG_BLOCK : ARG_LIST
    })

    this.state = {
      data: [],
      onPage: 1,
      currPage: 1,
      display: ARG_BLOCK,
      fetching: true,
      nodata: true
    }
  }

  componentDidMount() {
    this.props.getProjects()
  }
  componentWillReceiveProps(nextProps) {
    const oldData = this.state.data
    const newData = nextProps.project.list

    if (JSON.stringify(oldData) !== JSON.stringify(newData)) {
      this.setState({ data: newData })
    }
  }

  render() {
    return (
      <div className={`${styles.container} content-offset`}>
        <div className='section-title'>
          <h2>В прицеле</h2>
          <Input
            fluid
            icon='lnr lnr-magnifier'
            iconPosition='left'
            placeholder='Поиск квестов'
          />
        </div>
        <div className='filters'>
          <div>
            <Dropdown
              selection
              placeholder='За все время'
              options={optionsTime}
            />
            <Dropdown
              selection
              placeholder='Сначала сложные'
              options={optionsDifficulty}
            />
            <Dropdown
              selection
              placeholder='Сначала для команды'
              options={optionsTarget}
            />
          </div>
          <div>
            <Button.Group className={styles['grid-control']}>
              <Button
                icon='block layout'
                active={this.state.display === ARG_BLOCK}
                onClick={this.changeDisplayType}
              />
              <Button
                icon='list layout'
                active={this.state.display === ARG_LIST}
                onClick={this.changeDisplayType}
              />
            </Button.Group>
          </div>
        </div>
        <Masonry className={styles['block-items']} onClick={this.handleClick} options={optionsMasonry}>
          { this.getContentOnPage() }
        </Masonry>
        { this.getContentController() }
      </div>
    )
  }
  getContentOnPage() {
    const { data, onPage, currPage, display } = this.state
    const { fetching } = this.props.project
    let content = []

    if (fetching === true) {
      return (
        <Loader active inline='centered' size='huge'>
          Fetching
        </Loader>
      )
    }
    if (fetching === false && data.length === 0) {
      return (
        <Container textAlign='center'>
          <h2>No data found</h2>
        </Container>
      )
    }

    if (display === ARG_LIST) {
      const start = (currPage - 1) * onPage
      const end = start + onPage
      content = data.slice(start, end)
    } else {
      content = data
    }

    content = content.map(item => (
      <QuestsItem
        key={item.name}
        data={item}
        type={display}
        onSelect={this.handleSelectItem(item)}
      />
    ))

    return content
  }
  getContentController() {
    const { display, data, onPage } = this.state

    switch (true) {
      case display === ARG_LIST && data.length > onPage:
        return (
          <Pagination
            current={this.state.currPage}
            total={this.state.data.length}
            onChange={this.changePage}
            onDecrement={this.decCurrPage}
            onIncrement={this.incCurrPage}
          />
        )
      case display === ARG_BLOCK:
        return (
          <PaginationAdd />
        )
      default:
        return null
    }
  }
}

function mapStateToProps(state) {
  const { list, fetching } = state.rootReducer.project
  return {
    project: {
      list,
      fetching
    }
  }
}
function mapDispatchToProps(dispatch) {
  return {
    getProjects: bindActionCreators(getProjects, dispatch),
    selectProjectDetails: bindActionCreators(selectProjectDetails, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Quests)
