import * as c from 'constants/actions'

const initialState = {
  data: [],
  fetching: false,
  errors: {}
}

export default function project(state = initialState, action) {
  switch (action.type) {
    case c.API_ADD_PROPOSAL_REQUEST:
      return { ...state, fetching: true }

    case c.API_GET_PROPOSALS_SUCCESS:
      return { ...state, fetching: false, data: action.payload }
    case c.API_ADD_PROPOSAL_SUCCESS:
      return { ...state, fetching: false, data: state.data.concat(action.payload) }

    case c.API_ADD_PROPOSAL_ERROR:
      return { ...state, fetching: false, errors: action.payload }

    case c.API_LOGOUT_USER_SUCCESS:
      return initialState
    default:
      return state
  }
}
