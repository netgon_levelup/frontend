import {
  Menu, Header, Divider, Label
} from 'semantic-ui-react'

import { Sidebar as SidebarContainer } from 'components/containers'
import { redirect } from 'helpers/routing'
import { QUESTS_NEW } from 'constants/routes'

import styles from './sidebar-quests.module.scss'

const menuItems = [{
  name: 'Общие',
  items: [{
    name: 'Новые',
    label: 140
  }, {
    name: 'Выполненные',
    label: 140
  }]
}, {
  name: 'Личные',
  items: [{
    name: 'Созданные',
    label: 140
  }, {
    name: 'Выполненные',
    label: 140
  }, {
    name: 'В прицеле'
  }, {
    name: 'На выполнении'
  }]
}, {
  name: 'Команда',
  items: [{
    name: 'Созданные',
    label: 140
  }, {
    name: 'Выполненные',
    label: 140
  }, {
    name: 'В прицеле'
  }, {
    name: 'На выполнении'
  }]
}]
const SidebarQuests = () => (
  <SidebarContainer>
    <Menu vertical className={styles.menu}>
      <Header as='h2' className='content-offset page-title'>
        <Header.Content>
          Квесты
        </Header.Content>
      </Header>

      <Menu.Item
        className={styles.item}
        onClick={() => redirect(QUESTS_NEW)}
      >
        Добавить
      </Menu.Item>

      { menuItems.map((item) => {
        const divider = (<Divider horizontal className={styles['sidebar-section-divider']}>{ item.name }</Divider>)
        const items = item.items.map(sitem => (
          <Menu.Item className={styles.item} onClick={() => console.log(`${item.name} - ${sitem.name} clicked!`)}>
            { sitem.name }
            { sitem.label !== undefined && <Label>{ sitem.label }</Label>}
          </Menu.Item>
        ))
        return [divider, items]
      })}
    </Menu>
  </SidebarContainer>
)

export default SidebarQuests
