import React from 'react'
import { shallow } from 'enzyme'

import Example from '../src/components/routes/example/example.stateless.jsx'

describe('<Example />', () => {
  it('without props', () => {
    const fetching = shallow(<Example />)
    expect(fetching).toMatchSnapshot()
  })
  it('with props', () => {
    const showing = shallow(<Example ip='127.0.0.1' />)
    expect(showing).toMatchSnapshot()
  })
})
