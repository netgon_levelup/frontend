import * as c from 'constants/actions'

export function selectDetails(data) {
  return {
    type: c.SELECT_PROJECT_DETAILS,
    payload: data
  }
}

export function requestAdd(data) {
  return {
    type: c.API_ADD_PROJECT_REQUEST,
    payload: data
  }
}
export function successAdd(data) {
  return {
    type: c.API_ADD_PROJECT_SUCCESS,
    payload: data
  }
}
export function errorAdd(error) {
  return {
    type: c.API_ADD_PROJECT_ERROR,
    payload: error
  }
}

export function requestGet(id) {
  return {
    type: c.API_GET_PROJECT_REQUEST,
    payload: id
  }
}
export function successGet(data) {
  return {
    type: c.API_GET_PROJECT_SUCCESS,
    payload: data
  }
}
export function errorGet(error) {
  return {
    type: c.API_GET_PROJECT_ERROR,
    payload: error
  }
}

export function requestGetList(payload = {}) {
  return {
    type: c.API_GET_PROJECTS_REQUEST,
    payload
  }
}
export function successGetList(data) {
  return {
    type: c.API_GET_PROJECTS_SUCCESS,
    payload: data
  }
}
export function errorGetList(error) {
  return {
    type: c.API_GET_PROJECTS_ERROR,
    payload: error
  }
}
