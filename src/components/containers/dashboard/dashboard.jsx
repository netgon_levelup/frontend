import { connect } from 'react-redux'
import { Loader } from 'semantic-ui-react'

import { QUESTS, USER } from 'constants/routes'

import Header from './header/header-dashboard'
import SidebarQuests from './sidebar-quests/sidebar-quests'
import SidebarInventory from './sidebar-inventory/sidebar-inventory'
import Footer from './footer/footer'

import styles from './dashboard.module.scss'

class Dashboard extends React.PureComponent {
  render() {
    if (this.props.authenticated === false) {
      return (<Loader active inline='centered' size='massive' className={styles.spinner} />)
    }

    let sidebar
    const location = this.props.location.pathname
    switch (true) {
      case location.includes(QUESTS):
        sidebar = <SidebarQuests />
        break
      case location.includes(USER):
        sidebar = <SidebarInventory />
        break
      default:
        sidebar = null
    }

    return (
      <div>
        <Header />
        <div className={styles['content-container']}>
          { sidebar }
          <div className={styles['content-main']}>
            { this.props.children }
            <Footer />
          </div>
        </div>
      </div>
    )
  }
}

Dashboard.propTypes = {
  authenticated: PropTypes.bool.isRequired,

  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired
  }).isRequired,
  children: PropTypes.node.isRequired
}

function mapStateToProps(state) {
  return {
    authenticated: state.rootReducer.user.authenticated
  }
}
export default connect(mapStateToProps)(Dashboard)
