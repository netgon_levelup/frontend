import { call, put, takeEvery } from 'redux-saga/effects'

// import { redirect } from 'helpers/routing'
import * as c from 'constants/actions'
import * as a from 'actions/category'
import { filterByName, getAll } from './api/category'
// import * as r from 'constants/routes'

function* handleGet(action) {
  try {
    let result = ''
    if (action.payload.filter.length > 0) {
      result = yield call(filterByName(action.payload.filter))
    } else {
      result = yield call(getAll())
    }

    yield put(a.successGet(result))
    // redirect(r.LOGIN)
  } catch ({ message }) {
    yield put(a.errorGet(message))
  }
}

export default function* watcherUser() {
  yield takeEvery(c.API_GET_CATEGORY_REQUEST, handleGet)
}
