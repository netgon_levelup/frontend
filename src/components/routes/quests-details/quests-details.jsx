import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  Segment, Grid, Header, Image, Loader, Container,
  Divider, Icon, Form, Button, Message
} from 'semantic-ui-react'

import { SkillString } from 'components/commons'
import { getAvatarUrl, getDateStr } from 'helpers/'
import {
  requestGet as getProject
} from 'actions/project'
import {
  requestGetList as getProposals,
  requestAdd as addProposal
} from 'actions/proposal'

import styles from './quests-details.module.scss'

class QuestsDetails extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleProposal = e => this.setState({
      proposal: e.target.value, errors: { ...this.state.errors, proposal: false }
    })
    this.handleSubmit = ::this.handleSubmit

    this.state = {
      placeholder: 'Phasellus in erat tortor. Donec commodo metus in risus pulvinar, non aliquam massa congue. In nec neque semper, commodo purus facilisis, vehicula magna. Etiam neque massa, tempor vitae tempus eu, pellentesque nec velit. Duis quis elementum lacus. Etiam molestie nisl est, in sagittis quam suscipit at. Cras convallis, purus ut placerat malesuada, neque lectus pharetra est, ac euismod nisl ipsum faucibus velit.',
      proposal: '',
      errors: {
        proposal: false
      }
    }
  }
  componentDidMount() {
    const { id } = this.props.params
    if (Object.keys(this.props.project.data).length === 0) {
      this.props.getProject(id)
    }
    if (this.props.proposal.data.length === 0) {
      this.props.getProposals(id)
    }
  }
  render() {
    const { fetching, data } = this.props.project
    let content = null
    switch (true) {
      case fetching === true:
        content = (
          <Loader active inline='centered' size='huge'>
            Fetching
          </Loader>
        )
        break
      case Object.keys(data).length === 0:
        content = (
          <Container textAlign='center'>
            <h2>No data found</h2>
          </Container>
        )
        break
      default:
        content = this.getСontent()
    }
    return (
      <div className={`content-offset`}>
        <div className='section-title'>
          <h2>О квесте</h2>
        </div>
        { content }
      </div>
    )
  }
  getСontent() {
    const { data } = this.props.project
    const { user } = data

    if (user === undefined) {
      return null
    }

    return (
      <div>
        <Segment className={`panel ${styles.panel}`}>
          <Grid className={styles.header}>
            <Grid.Column>
              <Header as='h4'>
                <Image
                  size='small'
                  shape='circular'
                  className={`${styles.avatar} pointer`}
                  src={getAvatarUrl(user.details.avatar)}
                />
                <Header.Content>
                  <h5 className={styles.name}>{ user.username } <span>из</span> {'user'}</h5>
                  {/* <span>{ data.team.length > 0 && ` из ${data.team}` }</span> */}
                  <Header.Subheader>
                    <p className={`${styles.level} top-level hight`}>{`Требуемый уровень не ниже undef`}</p>
                    <p>{`Необходимый исполнитель: undef`}</p>
                  </Header.Subheader>
                </Header.Content>
              </Header>
            </Grid.Column>
            <Grid.Column className={styles.experiance}>
              <p>Опыт за квест:</p>
              <p>undef exp.</p>
            </Grid.Column>
          </Grid>
          <Divider />
          <p className={styles.dataCreated}>{ getDateStr(data.created) }</p>
          <div className={styles.panelBody}>
            <h2>{ data.name }</h2>
            <p>{ data.description }</p>
            <p>{ data.description }</p>
            <h3 className={styles.label}>Прикрепленные файлы:</h3>

            <div className={styles.files}>
              <div className={styles.file}>
                <Icon name='file image outline pink' size='large' />
                <label>img..jpg</label>
              </div>
              <div className={styles.file}>
                <Icon name='file word outline purple' size='large' />
                <label>doc..docx</label>
              </div>
              <div className={styles.file}>
                <Icon name='file excel outline green' size='large' />
                <label>excel..xlsx</label>
              </div>
              <div className={styles.file}>
                <Icon name='file archive outline orange' size='large' />
                <label>archive..zip</label>
              </div>
            </div>
          </div>
          <Divider />
          <SkillString
            items={data.skills}
          />
        </Segment>
        <div className='section-title'>
          <h2>Заявки участников:</h2>
        </div>
        <Segment className={`panel ${styles.panelRequest}`}>
          { this.getProposalErrors() }
          <Form className={styles.form}>
            <Form.TextArea
              label='Оставить заявку:'
              placeholder={this.state.placeholder}
              error={this.state.errors.proposal}
              value={this.state.proposal}
              onChange={this.handleProposal}
            />
          </Form>
          <Button color='yellow' className='submit' onClick={this.handleSubmit}>Отправить</Button>
          { this.getProposals() }
        </Segment>
      </div>
    )
  }
  getProposals() {
    const { data } = this.props.proposal
    if (data === undefined) {
      return null
    }

    return (
      <div className={styles.comments}>
        <Divider className='purple divider-title' horizontal>Заявок: { data.length }</Divider>
        { data.map(item => (
          <Grid key={item.id} className={styles.comment}>
            <Grid.Column floated='left' width={2}>
              <Image
                centered
                size='tiny'
                shape='circular'
                src={getAvatarUrl(item.user.details.avatar)}
              />
            </Grid.Column>
            <Grid.Column floated='right' width={14}>
              <Grid.Row>
                <Grid className={styles.commentHeader}>
                  <Grid.Column>
                    <h4 className={styles.name}>{ item.user.username }</h4>
                    <p className={`${styles.level} top-level hight`}>Уровень: { item.level }</p>
                  </Grid.Column>
                  <Grid.Column>
                    <p className={styles.date}>{ getDateStr(item.created, true) }</p>
                  </Grid.Column>
                </Grid>
              </Grid.Row>
              <Divider className={styles.topDivider} />
              <Grid.Row className={styles.desc}>
                <p>{ item.text }</p>
              </Grid.Row>
            </Grid.Column>
          </Grid>
        )) }
      </div>
    )
  }
  getProposalErrors() {
    const { errors } = this.props.proposal
    let detais = ''

    if (Object.keys(errors).length === 0) {
      return null
    }

    if (errors.uniqueness === true) {
      detais = 'Вы уже оставляли заявку для этого проекта'
    }

    return (
      <Message negative>
        <Message.Header>Ошибка при создании новой заявки</Message.Header>
        <p>{ detais }</p>
      </Message>
    )
  }
  handleSubmit() {
    const { proposal } = this.state
    if (proposal.length === 0) {
      return this.setState({ errors: { ...this.state.errors, proposal: true } })
    }

    return this.props.addProposal(proposal)
  }
}

QuestsDetails.propTypes = {
  project: PropTypes.shape({
    data: PropTypes.object.isRequired,
    fetching: PropTypes.bool.isRequired
  }).isRequired,
  proposal: PropTypes.shape({
    data: PropTypes.array.isRequired,
    fetching: PropTypes.bool.isRequired,
    errors: PropTypes.object.isRequired
  }).isRequired,
  params: PropTypes.shape({
    id: PropTypes.string.isRequired
  }).isRequired,

  getProject: PropTypes.func.isRequired,
  getProposals: PropTypes.func.isRequired,
  addProposal: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { project, proposal } = state.rootReducer
  return {
    project: {
      data: project.data,
      fetching: project.fetching
    },
    proposal
  }
}
function mapDispatchToProps(dispatch) {
  return {
    getProject: bindActionCreators(getProject, dispatch),
    addProposal: bindActionCreators(addProposal, dispatch),
    getProposals: bindActionCreators(getProposals, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(QuestsDetails)
