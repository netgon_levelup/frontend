import axios from 'helpers/axios'
// import { getToken } from './'

const BASE = 'skills'

export const getAll = () => async () => {
  const result = await axios.get(BASE)

  return result.data
}

export const filterByName = str => async () => {
  const result = await axios.get(`${BASE}?filter={"where":{"name":{"regexp":"/.?${str}.*/i"}}}`)

  return result.data
}
