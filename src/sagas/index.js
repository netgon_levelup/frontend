import { fork, all } from 'redux-saga/effects'

import watcherUser from './user'
import watcherSkill from './skill'
import watcherCategory from './category'
import watcherProject from './project'
import watcherProposal from './proposal'

export default function* rootSaga() {
  yield all([
    fork(watcherUser),
    fork(watcherSkill),
    fork(watcherCategory),
    fork(watcherProject),
    fork(watcherProposal)
  ])
}
