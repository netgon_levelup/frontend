import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Header, Grid, Image, Icon, Button, Progress, Popup } from 'semantic-ui-react'
import classNames from 'classnames'

import { Header as HeaderContainer } from 'components/containers'
import { TabsHeader } from 'components/commons'
import { redirect } from 'helpers/routing'
import { QUESTS, USER_PROFILE } from 'constants/routes'
import {
  requestLogout as logout
} from 'actions/user'

import styles from './header-dashboard.module.scss'

class DashboardHeader extends React.PureComponent {
  constructor(props) {
    super(props)

    this.redirectToQuests = () => redirect(QUESTS)
    this.redirectToProfile = () => redirect(USER_PROFILE)

    this.state = {}
  }
  render() {
    return (
      <HeaderContainer className={styles.container}>
        <Grid className={styles.row}>
          <Grid.Column className={styles['column-left']}>
            <Header>
              <a className='pull-right'>
                <Popup
                  trigger={
                    <Icon
                      name='info'
                      className='pointer icon-info'
                      color='purple'
                    />
                  }
                  position='right center'
                  content='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. '
                />
              </a>
              <Image
                name='plug'
                className={styles.logo}
                src='/assets/header-logo.png'
                onClick={this.redirectToQuests}
              />
              <Progress
                percent={60}
                indicating
                inverted
                className='striped'
                color='purple'
              />
              <div className={styles['left-skill']}>
                <span className={styles.level}>{ 'lvl 15' }</span>
                <p className={styles.specs}>
                  <span>{ '7 231' }</span>
                  <span>{ '10 000' }</span>
                </p>
              </div>
            </Header>
          </Grid.Column>
          <Grid.Column className={styles['column-center']}>
            <TabsHeader />
          </Grid.Column>
          <Grid.Column className={styles['column-right']}>
            <Header>
              <span
                className={classNames({
                  [styles.avatar]: true,
                  [styles.active]: false
                })}
              >
                <Image
                  shape='circular'
                  src='/assets/avatar.jpg'
                />
              </span>
              <Header.Content className={styles['r-block-center']}>
                <h4 className={styles.name}>
                  <span>{ 'Supemag' }</span>
                  { ' из ' }
                  <span>{ 'Gild of Mages' }</span>
                </h4>
                <Progress
                  percent={60}
                  indicating
                  inverted
                  className='striped'
                  color='pink'
                />
                <div className={styles['right-skill']}>
                  <span className={styles.level}>{ 'lvl 15' }</span>
                  <p className={styles.specs}>
                    <span>{ '7 231' }</span>
                    <span>{ '10 000' }</span>
                  </p>
                </div>
              </Header.Content>
              <Header.Content className={styles.buttons}>
                <Button className={styles.btn} onClick={this.redirectToProfile}>
                  <Icon name='user' className='icon-gap icon-User' />
                </Button>
                <Button className={styles.btn} onClick={this.props.logout}>
                  <Icon name='close' className='lnr lnr-cross' />
                </Button>
              </Header.Content>
            </Header>
          </Grid.Column>
        </Grid>
      </HeaderContainer>
    )
  }
}

DashboardHeader.propTypes = {
  logout: PropTypes.func.isRequired
}

function mapStateToProps() {
  return { }
}
function mapDispatchToProps(dispatch) {
  return {
    logout: bindActionCreators(logout, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DashboardHeader)
