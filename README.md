# Requirement installations
- [NodeJS](https://nodejs.org/en/)

## Running on development
- unpack from archive or clone from repository
- change directory to project folder
- ```npm install```
- configure configuration file in project_dir/config/default.json
- ```npm start```

## Running on production
- unpack from archive of clone from repository
- change directory to project folder
- set up ```NODE_ENV``` environment variable to ```production```
- ```npm install```
- ```npm run build```
- configure configuration file in project_dir/config/production.json
- ```npm start```