import { Segment, Grid, Icon, Divider, Table, Dropdown } from 'semantic-ui-react'
import moment from 'moment'
import classNames from 'classnames'

import { ProfileHeader } from 'components/commons'

import styles from './user-gold.module.scss'

const optionsTime = [{ key: 'all', value: 'all', text: 'За все время' }]

const tableData = [{
  id: 1,
  vector: 0,
  created: moment().format('DD.MM.YYYY'),
  project: {
    name: 'Нужен дизайн сайта'
  },
  sum: '5 235'
}, {
  id: 2,
  vector: 1,
  created: moment().format('DD.MM.YYYY'),
  name: 'карта Сбербанка',
  sum: '5 235'
}, {
  id: 3,
  vector: 0,
  created: moment().format('DD.MM.YYYY'),
  project: {
    name: 'Нужен дизайн сайта'
  },
  sum: '5 235'
}, {
  id: 4,
  vector: 1,
  created: moment().format('DD.MM.YYYY'),
  name: 'карта Сбербанка',
  sum: '5 235'
}]

export default class UserGold extends React.PureComponent {
  state = {}
  render() {
    return (
      <div className='content-offset'>
        <div className='section-title'>
          <h2>
            Золото
          </h2>
        </div>
        <ProfileHeader
          avatar={'default-avatar-knives-ninja.png'}
          username={'абвгдейка'}
          level={15}
          experience={5252}
        />
        <Segment raised className={`${styles.panel} panel`}>
          <Grid className={styles.panelHeader}>
            <Grid.Column>
              <h3>История транзакций</h3>
            </Grid.Column>
            <Grid.Column className={styles.balance}>
              <Icon name='money' className='icon-gap icon-Dollars' />
              <span>Доступно для снятия: </span>
              <p className={styles.right}>
                <span className={styles.sum}>35056</span>
                <span className={styles.currency}> р.</span>
              </p>
            </Grid.Column>
          </Grid>
          <Divider />

          <div className={`${styles.filter} filters`}>
            <Dropdown
              selection
              placeholder='За все время'
              options={optionsTime}
            />
          </div>

          <Table className={styles.table}>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Операции</Table.HeaderCell>
                <Table.HeaderCell>Дата</Table.HeaderCell>
                <Table.HeaderCell>Сумма</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              { tableData.map((item) => {
                const inbound = item.vector === 0
                return (
                  <Table.Row key={item.id}>
                    <Table.Cell>
                      { inbound === true
                        ? (
                          <div className={styles['inbound-operation']}>
                            <i className={styles.iconPlus}>+ </i>
                            <span>Компенсация за проект: <a>{ item.project.name }</a></span>
                          </div>
                        )
                        : (
                          <div className={styles['outbound-operation']}>
                            <i className={styles.iconMinus}>- </i>
                            <span>Вывод средств: { item.name }</span>
                          </div>
                        )
                      }
                    </Table.Cell>
                    <Table.Cell>{ item.created }</Table.Cell>
                    <Table.Cell
                      className={classNames({
                        [styles['inbound-sum']]: inbound === true,
                        [styles['outbound-sum']]: inbound === false
                      })}
                    >
                      <span>{ inbound === true ? '+ ' : '- ' }</span>
                      <span>{ item.sum }</span>
                      <span> р.</span>
                    </Table.Cell>
                  </Table.Row>
                )
              })}
            </Table.Body>
          </Table>
        </Segment>
      </div>
    )
  }
}
