import { Icon } from 'semantic-ui-react'

import styles from './pagination-add.module.scss'

const PaginationAdd = () => (
  <div className={styles.container}>
    <div className={styles.wrapper}>
      <Icon.Group size='huge' className='icon-add-plus pink'>
        {/* <Icon name='circle outline' color='red' className='icon-with-circle' />
        <Icon name='add' size='tiny' color='red' /> */}
      </Icon.Group>
      <p className={styles['add-text']}>Смотреть еще</p>
    </div>
  </div>
)

export default PaginationAdd
