import { Segment, Grid, Icon, Divider, List, Button } from 'semantic-ui-react'

import { ProfileHeader } from 'components/commons'

import styles from './user-level.module.scss'

const data = [{
  id: 0,
  name: 'Основы веб-дизайна',
  percent: 98
}, {
  id: 1,
  name: 'Основы программирования на C++',
  percent: 10
}, {
  id: 22,
  name: 'Основы верстки (front-end)'
}, {
  id: 23,
  name: 'Основы верстки (front-end)'
}, {
  id: 24,
  name: 'Основы верстки (front-end)'
}]

export default class UserGold extends React.PureComponent {
  state = {}
  render() {
    return (
      <div className='content-offset'>
        <div className='section-title'>
          <h2>
            Уровень
          </h2>
        </div>
        <ProfileHeader
          avatar={'default-avatar-knives-ninja.png'}
          username={'абвгдейка'}
          level={15}
          experience={5252}
        />
        <Segment raised className={`${styles.panel} panel`}>
          <Grid className={styles.panelHeader}>
            <Grid.Column>
              <h3>Арена</h3>
            </Grid.Column>
            <Grid.Column className={styles.experience}>
              <Icon name='truck' className='icon-gap icon-Cup' />
              <span>Опыт за ответы: </span>
              <span className={styles.number}>4256</span>
            </Grid.Column>
          </Grid>
          <Divider />
          <h4>Тесты</h4>
          <List divided verticalAlign='middle'>
            { data.map((item) => {
              let content
              let button
              let text
              if (item.percent !== undefined) {
                text = `${item.name} (${item.percent})`
                button = (
                  <Button basic color='purple'>
                    Пройти тест еще раз
                    <Icon name='long arrow right' />
                  </Button>
                )
              } else {
                text = `${item.name}`
                button = (
                  <Button basic color='pink'>
                    Пройти тест
                    <Icon name='long arrow right' />
                  </Button>
                )
              }
              switch (true) {
                case item.percent !== undefined && item.percent > 50:
                  content = (
                    <div className={styles['test-success']}>
                      <Icon name='pencil' className='icon-gap icon-Pen' />
                      <span className={styles.text}>{ text }</span>
                      <Icon name='checkmark' />
                    </div>
                  )
                  break
                case item.percent !== undefined && item.percent < 50:
                  content = (
                    <div className={styles['test-failed']}>
                      <Icon name='id card outline' className='icon-gap icon-Blog' />
                      <span className={styles.text}>{ text }</span>
                      <Icon name='minus' />
                    </div>
                  )
                  break
                default:
                  content = (
                    <div>
                      <Icon name='code' />
                      { text }
                    </div>
                  )
              }
              return (
                <List.Item key={item.id}>
                  <List.Content floated='right'>
                    { button }
                  </List.Content>
                  <List.Content>
                    { content }
                  </List.Content>
                </List.Item>
              )
            })}
          </List>
        </Segment>
      </div>
    )
  }
}
