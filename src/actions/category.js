import * as c from 'constants/actions'

export function requestGet(data = {}) {
  return {
    type: c.API_GET_CATEGORY_REQUEST,
    payload: {
      filter: data.filter || ''
    }
  }
}
export function successGet(data) {
  return {
    type: c.API_GET_CATEGORY_SUCCESS,
    payload: data
  }
}
export function errorGet(error) {
  return {
    type: c.API_GET_CATEGORY_ERROR,
    payload: error
  }
}
