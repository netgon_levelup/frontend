import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { browserHistory } from 'react-router'
import { routerMiddleware, routerReducer } from 'react-router-redux'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootReducer from 'reducers/'
import rootSaga from 'sagas/'

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware()
  const middleware = [routerMiddleware(browserHistory), sagaMiddleware]

  const store = createStore(
    combineReducers({
      rootReducer,
      routing: routerReducer
    }),
    window.isProduction
      ? applyMiddleware(...middleware)
      : composeWithDevTools(applyMiddleware(...middleware))
  )

  /*
  if (window.isProduction === false) {
    // eslint-disable-next-line no-unused-vars,react/no-deprecated
    let createClass = React.createClass
    Object.defineProperty(React, 'createClass', {
      set: (nextCreateClass) => {
        createClass = nextCreateClass
      }
    })
    // eslint-disable-next-line global-require
    const { whyDidYouUpdate } = require('why-did-you-update')
    whyDidYouUpdate(React, { exclude: /^Form/ })
  }
  */

  if (module.hot) {
    module.hot.accept('reducers/', () => {
      const nextRootReducer = require('reducers/').default
      store.replaceReducer(nextRootReducer)
    })
  }

  sagaMiddleware.run(rootSaga)

  return store
}
