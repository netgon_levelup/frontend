import { Grid, Segment, Image, Progress } from 'semantic-ui-react'

import { getAvatarUrl } from 'helpers/'

import styles from './profile-header.module.scss'

const ProfileHeader = ({ avatar, username, level, experience }) => (
  <Segment raised className={`${styles.panelTop} panel`}>
    <Grid>
      <Grid.Column>
        <Image
          centered
          size='medium'
          shape='circular'
          className={styles.avatar}
          src={getAvatarUrl(avatar)}
        />
      </Grid.Column>
      <Grid.Column width={16}>
        <Grid.Row>
          <Grid verticalAlign='middle'>
            <Grid.Column>
              <h4 className={styles.name}>{ username }</h4>
            </Grid.Column>
          </Grid>
        </Grid.Row>
        <Grid.Row className={styles.skills}>
          <Grid>
            <Grid.Column className={styles['progress-row']} width={16}>
              <Progress
                percent={60}
                indicating
                inverted
                className='striped'
                color='purple'
              />
              <div className={styles['skills-details']}>
                <p className={styles.level}>LVL { level }</p>
                <p className={styles.detail}>{ experience } / undef</p>
              </div>
            </Grid.Column>
          </Grid>
        </Grid.Row>
      </Grid.Column>
    </Grid>
  </Segment>
)

ProfileHeader.propTypes = {
  avatar: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  level: PropTypes.number.isRequired,
  experience: PropTypes.number.isRequired
}

export default ProfileHeader
