import { ENV_PRODUCTION, STORAGE_USER_AVATAR } from 'constants/index'
import moment from 'moment'

export function setupEnvironment() {
  window.isProduction = (process.env.NODE_ENV === ENV_PRODUCTION)
}

export function convertCanvasToFile(canvas, filename) {
  const imgB64 = canvas.toDataURL('image/png')
  const png = imgB64.split(',')[1]

  const blob = new Blob([window.atob(png)], { type: 'image/png', encoding: 'utf-8' })
  const file = new File([blob], filename)
  file.preview = imgB64

  return file
}

export function getImageUrl(container, image) {
  return `${Config.backend.url}/storages/${container}/download/${image}`
}
export function getAvatarUrl(image, clear = false) {
  if (clear === true) {
    return image
  }

  return getImageUrl(STORAGE_USER_AVATAR, image)
}

export function getDateStr(date, time = false) {
  let format = 'DD MMMM YYYY'
  if (time === true) {
    format = `${format}, HH:mm`
  }
  return moment(date).format(format).toUpperCase()
}

export function isEmailValid(string) {
  // eslint-disable-next-line
  return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(string)
}
export function isPasswordValid(string) {
  return string.length >= 8
}

export function handleInput(label, timetrace = false) {
  const setState = ::this.setState
  return function (e) {
    if (timetrace === true) console.time(`handle input ${label}`)
    setState({
      [label]: e.target.value
    }, () => timetrace === true && console.timeEnd(`handle input ${label}`))
  }
}
