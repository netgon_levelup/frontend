const { resolve } = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const fbFixes = require('postcss-flexbugs-fixes')
const config = require('config')

const PRODUCTION = 'production'

const loaderPostCss = {
  loader: 'postcss-loader',
  options: {
    sourceMap: true,
    indent: 'postcss',
    plugins: () => [
      fbFixes,
      autoprefixer({
        browsers: [
          '>1%',
          'last 4 versions',
          'Firefox ESR',
          'not ie < 9' // React doesn't support IE8 anyway
        ],
        flexbox: 'no-2009'
      })
    ]
  }
}
const loaderStyle = {
  loader: 'style-loader'
}
const loaderSass = {
  loader: 'sass-loader'
}

module.exports = function (isProduction = (process.env.NODE_ENV === PRODUCTION)) {
  const result = {
    entry: [],
    output: {},
    module: {
      rules: []
    },
    plugins: [
      new webpack.ProvidePlugin({
        React: 'react',
        PropTypes: 'prop-types'
      })
    ],
    resolve: {}
  }
  if (isProduction) {
    // PRODUCTION SETTINGS
    result.plugins = result.plugins.concat([
      new webpack.IgnorePlugin(/\.\/dev/, /\/config$/),
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(PRODUCTION)
        },
        Config: JSON.stringify(config)
      }),
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        minimize: true,
        output: {
          comments: false
        }
      }),
      new webpack.optimize.ModuleConcatenationPlugin()
    ])
  } else {
    // DEVELOPMENT SETTINGS
    result.devtool = 'cheap-module-eval-source-map'
    result.entry = result.entry.concat([
      'webpack-hot-middleware/client'
    ])
    result.module.rules = result.module.rules.concat([{
      test: /\.(js|jsx)$/,
      loaders: ['eslint-loader'],
      include: [resolve(__dirname, 'src')],
      exclude: /(node_modules)/,
      enforce: 'pre'
    }])
    result.plugins = result.plugins.concat([
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
      new webpack.DefinePlugin({
        Config: JSON.stringify(config)
      })
    ])
  }
  // GENERAL SETTINGS
  result.output = {
    path: resolve(__dirname, './static/dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  }
  result.entry = result.entry.concat([
    'babel-polyfill',
    './src/index'
  ])
  result.module.rules = result.module.rules.concat([
    {
      test: /\.(js|jsx)$/,
      loaders: ['babel-loader'],
      exclude: [/node_modules/]
    }, {
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    }, {
      test: /\.module.scss$/,
      use: [
        loaderStyle,
        {
          loader: 'css-loader',
          options: {
            importLoaders: 2,
            modules: true,
            sourceMap: true,
            camelCase: true,
            localIdentName: '[name]__[local]___[hash:base64:5]'
          }
        },
        loaderPostCss,
        loaderSass
      ]
    }, {
      test: /^((?!\.module).)*scss$/,
      use: [
        loaderStyle,
        {
          loader: 'css-loader',
          options: {
            importLoaders: 2
          }
        },
        loaderPostCss,
        loaderSass
      ]
    }, {
      test: /\.html$/,
      loader: 'file-loader?name=[path][name].[ext]!extract-loader!html-loader'
    }, {
      test: /\.md$/,
      loader: 'html!markdown?gfm=true&tables=true&breaks=false&pedantic=false&sanitize=false&smartLists=true&smartypants=false'
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file-loader'
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=application/octet-stream'
    }, {
      test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=application/font-woff'
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file-loader'
    }
  ])
  result.resolve = {
    modules: [
      resolve('./src'),
      'node_modules',
      'src',
      'static/assets'
    ],
    extensions: ['.js', '.jsx', '.json'],
    alias: { }
  }

  return result
}
