import { Tab } from 'semantic-ui-react'

import styles from './header-tabs.module.scss'

const panes = [
  { menuItem: 'КВЕСТЫ' },
  { menuItem: 'ФРИЛАНСЕРЫ' },
  { menuItem: 'ЗАКАЗЧИКИ' }
]

const TabsHeader = () => (<Tab menu={{ secondary: false }} panes={panes} className={styles['nav-menu']} />)

export default TabsHeader
