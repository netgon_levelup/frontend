import styles from './skill-string.module.scss'

const SkillString = props => (
  <div className={styles.skills}>
    <span className={styles.title}>Скиллы: </span>
    { props.items.map((item, i) => {
      const name = `${item.name}${i + 1 < props.items.length ? ', ' : ''}`
      if (item.link === true) {
        return (<a key={name}>{name}</a>)
      }
      return (<span key={name}>{name}</span>)
    })}
  </div>
)

SkillString.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired
  })).isRequired
}

export default SkillString
