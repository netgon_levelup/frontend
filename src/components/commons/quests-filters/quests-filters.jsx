import { Input, Button, Dropdown } from 'semantic-ui-react'

import { ARG_LIST, ARG_BLOCK } from 'constants/index'

import styles from './quests-filters.module.scss'

const optionsTime = [{ key: 'all', value: 'all', text: 'За все время' }]
const optionsDifficulty = [{ key: 'hard', value: 'hard', text: 'Сначала сложные' }]
const optionsTarget = [{ key: 'team', value: 'team', text: 'Сначала для команды' }]

const QuestsFilters = props => (
  <div>
    <Input
      fluid
      className={styles.search}
      icon='lnr lnr-magnifier'
      iconPosition='left'
      placeholder='Поиск квестов'
    />
    <div className='filters'>
      <div>
        <Dropdown
          selection
          placeholder='За все время'
          options={optionsTime}
        />
        <Dropdown
          selection
          placeholder='Сначала сложные'
          options={optionsDifficulty}
        />
        <Dropdown
          selection
          placeholder='Сначала для команды'
          options={optionsTarget}
        />
      </div>
      <div>
        <Button.Group className={styles['grid-control']}>
          <Button
            icon='block layout'
            active={props.displayType === ARG_BLOCK}
            onClick={props.handleDisplayType}
          />
          <Button
            icon='list layout'
            active={props.displayType === ARG_LIST}
            onClick={props.handleDisplayType}
          />
        </Button.Group>
      </div>
    </div>
  </div>
)

QuestsFilters.propTypes = {
  displayType: PropTypes.oneOf([ARG_BLOCK, ARG_LIST]).isRequired,
  handleDisplayType: PropTypes.func.isRequired
}

export default QuestsFilters
