import { Divider, Icon, Grid } from 'semantic-ui-react'

import styles from './footer.module.scss'

const icons = [{
  name: 'facebook f'
}, {
  name: 'twitter'
}, {
  name: 'google plus'
}, {
  name: 'linkedin'
}]

const Footer = () => (
  <div className={styles.container}>
    <Divider />
    <div className={styles.top}>
      <div>
        <h3>О САЙТЕ</h3>
        <p><a href='#'>Помощь</a></p>
        <p><a href='#'>Правила</a></p>
        <p><a href='#'>Команда</a></p>
      </div>
      <div>
        <h3>СЕРВИСЫ</h3>
        <p><a href='#'>Поиск по сайту</a></p>
        <p><a href='#'>Безопасная сделка</a></p>
        <p><a href='#'>Платные опции в проектах</a></p>
      </div>
      <div>
        <h3>ПРОФИЛЬ</h3>
        <p><a href='#'>Войти</a></p>
        <p><a href='#'>Зарегестрироваться</a></p>
        <p><a href='#'>Восстановить доступ</a></p>
      </div>
    </div>
    <Divider />

    <Grid className={styles.bottom} verticalAlign='middle'>
      <Grid.Column mobile={16} tablet={8} computer={8}>
        <span className={styles.copyright}>© LevelUp 2017</span>
      </Grid.Column>
      <Grid.Column mobile={16} tablet={8} computer={8} textAlign='right'>
        <div className={styles['social-wrapper']}>
          { icons.map(item => (
            <Icon.Group as='a' href='#' size='big' onClick={() => console.log(item.name, 'clicked!')} key={item.name}>
              {/* <Icon name='circle outline' className='icon-with-circle' /> */}
              <Icon name={item.name} size='tiny' />
            </Icon.Group>
          ))}
        </div>
      </Grid.Column>
    </Grid>
  </div>
)

export default Footer
