import Header from './header/header-lobby'

const Lobby = props => (
  <div>
    <Header />
    { props.children }
  </div>
)

Lobby.propTypes = {
  children: PropTypes.node.isRequired
}

export default Lobby
