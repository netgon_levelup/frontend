import axios from 'helpers/axios'
import {
  STORAGE_USER_AVATAR,
  STORAGE_PROJECT_FILES
  // STORAGE_USER_FILES
} from 'constants/index'

const BASE = 'storages'

export const uploadAvatar = form => async () => {
  const result = await axios.post(`${BASE}/${STORAGE_USER_AVATAR}/upload`, form)

  return result.data
}

export const uploadProjectFile = form => async () => {
  const result = await axios.post(`${BASE}/${STORAGE_PROJECT_FILES}/upload`, form)

  return result.data
}
