import axios from 'helpers/axios'

const BASE = 'projects'
function getFilter({ skip = 0, limit = 10 }) {
  const filter = {
    include: [{
      relation: 'user',
      scope: {
        include: 'details'
      }
    }, {
      relation: 'skills'
    }]
  }
  if (isNaN(skip) === false) {
    filter.skip = skip < 0 ? 0 : skip
  }
  if (isNaN(limit) === false) {
    filter.limit = limit < 1 ? 10 : limit
  } else {
    filter.limit = 10
  }
  return `filter=${JSON.stringify(filter)}`
}

export const add = params => async () => {
  const result = await axios.post(BASE, params)

  return result.data
}

export const get = id => async () => {
  const result = await axios.get(`${BASE}/${id}/?${getFilter()}`)

  return result.data
}

export const getList = query => async () => {
  const result = await axios.get(`${BASE}/?${getFilter(query)}`)

  return result.data
}
export const getCount = () => async () => {
  const result = await axios.get(`${BASE}/count/`)

  return result.data
}
