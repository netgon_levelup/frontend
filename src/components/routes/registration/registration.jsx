import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  Segment, Header, Image, Divider, Button, Icon, Modal, Form
} from 'semantic-ui-react'
import Cropper from 'react-cropper'

import { Dropzone, FilesAdd } from 'components/commons'
import { NO_IMAGE_SELECTED } from 'constants/index'
import { convertCanvasToFile, isEmailValid, isPasswordValid, handleInput } from 'helpers/'
import { requestRegister } from 'actions/user'

import styles from './registration.module.scss'

class Registration extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleSubmit = ::this.handleSubmit
    this.handleAvatarPreviewDrop = file => this.setAvatar(file[0], 'preview')
    this.handleAvatarPreviewClear = () => this.setAvatar(null, 'preview')
    this.handleAvatarOpenModal = () => this.setAvatar(true, 'isModalOpen')
    this.handleAvatarCloseModal = () => this.setAvatar(false, 'isModalOpen')
    this.handleAvatarSetSelected = () => this.setAvatarSelected()

    this.handleEmail = e => this.setState({
      email: e.target.value,
      errors: { ...this.state.errors, email: !isEmailValid(e.target.value) }
    })
    this.handlePassword = e => this.setState({
      password: e.target.value,
      errors: { ...this.state.errors,
        password: !isPasswordValid(e.target.value),
        retyped: this.state.password !== e.target.value
      }
    })
    this.handleRetyped = e => this.setState({
      retyped: e.target.value,
      errors: {
        ...this.state.errors,
        retyped: !isPasswordValid(e.target.value) || this.state.password !== e.target.value
      }
    })

    this.handleUsername = handleInput.bind(this)('username')
    this.handleFirstName = handleInput.bind(this)('firstName')
    this.handleLastName = handleInput.bind(this)('lastName')
    this.handleMiddleName = handleInput.bind(this)('middleName')

    this.handlePassportNumber = e => this.setPassport(e, 'number')
    this.handlePassportSerial = e => this.setPassport(e, 'serial')
    this.handlePassportAddImage = ::this.handlePassportAddImage
    this.handlePassportRemoveImage = i => () => this.setPassportImage(
      this.state.passport.images.filter((el, j) => j !== i),
      true
    )

    this.handleInvertAgreed = () => this.setState({ agreed: !this.state.agreed })

    this.state = {
      email: '',
      password: '',
      retyped: '',
      username: '',
      firstName: '',
      lastName: '',
      middleName: '',
      passport: {
        number: '',
        serial: '',
        images: []
      },
      agreed: true,
      avatar: {
        selected: null,
        preview: null,
        isModalOpen: false
      },
      errors: {
        username: false,
        email: false,
        password: false,
        retyped: false
      }
    }
  }


  render() {
    const { state } = this
    const serialAdditional = {}
    const selected = state.avatar.selected
    if (state.passport.serial.length !== 0) {
      serialAdditional.icon = 'checkmark'
      serialAdditional.iconPosition = 'right'
    }
    return (
      <Segment className='form_container'>
        <Header as='h2' icon textAlign='center'>
          <Header.Content>
            Регистрация на LEVELUP
          </Header.Content>
        </Header>
        <Image
          centered
          size='small'
          shape='circular'
          className='pointer drop-avatar'
          src={selected !== null ? selected.preview : NO_IMAGE_SELECTED}
          onClick={this.handleAvatarOpenModal}
        />
        { this.getChangeAvatarModal() }
        <Form>
          <Form.Group widths='equal'>
            <Form.Input
              label='Имя пользователя'
              error={state.errors.username}
              value={state.username}
              onChange={this.handleUsername}
            />
            <Form.Input
              required
              label='Email'
              error={state.errors.email}
              value={state.email}
              onChange={this.handleEmail}
            />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Input
              required
              label='Пароль'
              type='password'
              error={state.errors.password}
              value={state.password}
              onChange={this.handlePassword}
            />
            <Form.Input
              required
              label='Повторите пароль'
              type='password'
              error={state.errors.retyped}
              value={state.retyped}
              onChange={this.handleRetyped}
            />
          </Form.Group>
        </Form>

        <Divider horizontal className={styles.divider}>Персональная информация</Divider>

        <Form>
          <Form.Input
            label='Имя'
            value={state.firstName}
            onChange={this.handleFirstName}
          />
          <Form.Input
            label='Фамилия'
            value={state.lastName}
            onChange={this.handleLastName}
          />
          <Form.Input
            label='Отчество'
            value={state.middleName}
            onChange={this.handleMiddleName}
          />
          <Form.Group>
            <Form.Input
              label='Номер паспорта'
              width={12}
              value={state.passport.number}
              onChange={this.handlePassportNumber}
            />
            <Form.Input
              label='Серия'
              width={4}
              value={state.passport.serial}
              onChange={this.handlePassportSerial}
            />
          </Form.Group>
        </Form>

        <p className='text-center text-title'>Загрузите фото своего паспорта и вас с паспортом</p>
        <FilesAdd
          onDrop={this.handlePassportAddImage}
          accept='image/jpeg, image/png'
          files={state.passport.images}
          onRemoveTag={this.handlePassportRemoveImage}
        />
        <Form.Checkbox
          className={styles['form-agreed']}
          label='С правилами работы сервиса ознакомлен и согласен.'
          checked={state.agreed}
          onClick={this.handleInvertAgreed}
        />

        <Button
          color='yellow'
          className='submit'
          fluid
          loading={this.props.user.fetching}
          onClick={this.handleSubmit}
        >
          ЗАРЕГЕСТРИРОВАТЬСЯ
        </Button>

      </Segment>
    )
  }
  getChangeAvatarModal() {
    const { avatar } = this.state
    if (avatar.isModalOpen === true) {
      return (
        <Modal open basic size='small'>
          <Modal.Content>
            { avatar.preview !== null
              ? (<Cropper
                ref={(ref) => { this.cropper = ref }}
                className={styles['avatar-dz']}
                src={avatar.preview.preview}
                aspectRatio={1 / 1}
                guides={false}
              />)
              : (<Dropzone
                className={styles['avatar-dz']}
                onDrop={this.handleAvatarPreviewDrop}
                accept='image/jpeg, image/png'
              />)
            }
          </Modal.Content>
          <Modal.Actions>
            {avatar.preview && <Button
              inverted
              onClick={this.handleAvatarPreviewClear}
            >
              <Icon name='edit' /> Clear
            </Button>}
            <Button
              basic
              color='red'
              onClick={this.handleAvatarCloseModal}
            >
              <Icon name='remove' /> Cancel
            </Button>
            <Button
              color='green'
              onClick={this.handleAvatarSetSelected}
            >
              <Icon name='checkmark' /> Save
            </Button>
          </Modal.Actions>
        </Modal>
      )
    }
    return null
  }
  setName(e, field) {
    this.setSubField(e.target.value, 'name', field)
  }
  setPassport(e, field) {
    this.setSubField(e.target.value, 'passport', field)
  }
  setPassportImage(value, replace = false) {
    this.setSubField(
      replace === false ? this.state.passport.images.concat([value]) : value,
      'passport',
      'images'
    )
  }
  setAvatar(value, field) {
    this.setSubField(value, 'avatar', field)
  }
  setAvatarSelected() {
    const file = convertCanvasToFile(this.cropper.getCroppedCanvas(), 'avatar.jpg')
    this.setState({
      avatar: {
        ...this.state.avatar,
        isModalOpen: false,
        selected: file
      }
    })
  }
  setError(field, value = true) {
    this.setSubField(value, 'errors', field)
  }
  setSubField(value, item, field) {
    this.setState({ [item]: { ...this.state[item], [field]: value } })
  }


  handlePassportAddImage(files) {
    const file = files[0]
    const exist = !!this.state.passport.images.find(item => item.name === file.name)
    if (exist === false) this.setPassportImage(file)
  }

  handleSubmit() {
    const {
      username, email, password, retyped, agreed,
      firstName, lastName, middleName, avatar
    } = this.state

    if (agreed === false) return null

    const errors = {}
    if (username.length === 0) {
      errors.username = true
    }
    if (!isEmailValid(email)) {
      errors.email = true
    }
    if (!isPasswordValid(password) || password !== retyped) {
      errors.password = true
    }
    if (Object.keys(errors).length !== 0) {
      return this.setState({ errors })
    }

    const data = {
      username,
      email,
      password,
      details: {
        avatar: avatar.selected,
        firstName,
        lastName,
        middleName
      }
    }
    return this.props.requestRegister(data)
  }
}

Registration.propTypes = {
  user: PropTypes.shape({
    fetching: PropTypes.bool.isRequired
  }).isRequired,

  requestRegister: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    user: state.rootReducer.user
  }
}
function mapDispatchToProps(dispatch) {
  return {
    requestRegister: bindActionCreators(requestRegister, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Registration)
