import { Label, Icon } from 'semantic-ui-react'
import { Dropzone } from 'components/commons'

import styles from './files-add.module.scss'


class FilesAdd extends React.PureComponent {
  shouldComponentUpdate(nextProps) {
    if (JSON.stringify(this.props.files) === JSON.stringify(nextProps.files)) {
      return false
    }
    return true
  }
  render() {
    return (
      <div>
        <Dropzone
          onDrop={this.props.onDrop}
          accept={this.props.accept}
        >
          <div className={styles.banner}>
            <Icon name='cloud upload' size='huge' />
            <p>DRAG & DROP</p>
            <p>your files, or <a>browse</a></p>
          </div>
        </Dropzone>
        { this.props.files.map((item, i) => (
          <Label className={styles.tag} key={item.name}>
            <Icon name='image' />
            <span>{item.name}</span>
            <Icon
              name='delete'
              onClick={this.props.onRemoveTag(i)}
            />
          </Label>
        ))}
      </div>
    )
  }
}


FilesAdd.defaultProps = {
  accept: '*'
}

FilesAdd.propTypes = {
  onDrop: PropTypes.func.isRequired,
  onRemoveTag: PropTypes.func.isRequired,
  files: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired
  })).isRequired,

  accept: PropTypes.string
}

export default FilesAdd
