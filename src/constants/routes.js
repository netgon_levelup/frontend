export const INDEX = '/'
export const ANY = '*'

export const LOGIN = '/login'
export const REGISTRATION = '/registration'

export const QUESTS = '/quests'
export const QUESTS_NEW = `${QUESTS}/new`

export const USER = '/user'
export const USER_PROFILE = `${USER}/profile`
export const USER_SKILLS = `${USER}/skills`
export const USER_LEVEL = `${USER}/level`
export const USER_EXPERIENCE = `${USER}/experience`
export const USER_GOLD = `${USER}/gold`
