
import { Segment, Grid, Dropdown, Icon, Header, Divider, Table } from 'semantic-ui-react'
import { ProfileHeader, ProfileBarChart } from 'components/commons'
import moment from 'moment'
import _ from 'lodash'

import styles from './user-experience.module.scss'

const ddOptions = [
  { key: 1, text: 'Choice 1', value: 1 },
  { key: 2, text: 'Choice 2', value: 2, disabled: true },
  { key: 3, text: 'Choice 3', value: 3 }
]

const COL_NAME = 'name'
const COL_DATE = 'created'
const COL_EXP = 'experience'

const DIR_ASC = 'ascending'
const DIR_DES = 'descending'

export default class UserExperience extends React.PureComponent {
  constructor(props) {
    super(props)

    this.handleSortName = () => this.handleSort(COL_NAME)
    this.handleSortDate = () => this.handleSort(COL_DATE)
    this.handleSortExp = () => this.handleSort(COL_EXP)

    this.state = {
      data: [{
        id: 0,
        name: 'Нужен дизайн сайта',
        created: moment().format('DD.MM.YYYY'),
        experience: 250
      }, {
        id: 1,
        name: 'Программист на angular',
        created: moment().format('DD.MM.YYYY'),
        experience: 570
      }, {
        id: 2,
        name: 'Иллюстрация для ИМ',
        created: moment().format('DD.MM.YYYY'),
        experience: 1200
      }, {
        id: 3,
        name: 'Программист на C#',
        created: moment().format('DD.MM.YYYY'),
        experience: 2999
      }],
      column: '',
      direction: ''
    }
  }
  render() {
    const { data, column, direction } = this.state
    return (
      <div className='content-offset'>
        <div className='section-title'>
          <h2>
            Опыт
          </h2>
        </div>
        <ProfileHeader
          avatar={'default-avatar-knives-ninja.png'}
          username={'абвгдейка'}
          level={15}
          experience={5252}
        />
        <Segment className='panel'>
          <Grid>
            <Grid.Column floated='left' width={5}>
              <h3>опыт</h3>
            </Grid.Column>
            <Grid.Column>
              <div className={`${styles.chartPeriod}`}>
                <Dropdown text='Месяц 2017' options={ddOptions} />
              </div>
            </Grid.Column>
          </Grid>
          <ProfileBarChart />
        </Segment>
        <Segment className='panel'>
          <Grid className={styles.panelStatistic}>
            <Grid.Column className={styles.left}>
              <Header>
                <Icon name='user' className='icon-gap icon-User' />
                <p><span>Личный:</span><span className={styles.exp}> 235</span></p>
              </Header>
              <Header>
                <Icon name='talk' className='icon-gap icon-Message' />
                <p><span>Арена:</span><span className={styles.exp}> 235</span></p>
              </Header>
              <Header>
                <Icon name='users' className='icon-gap icon-gap icon-Users' />
                <p><span>Команда:</span><span className={styles.exp}> 235</span></p>
              </Header>
            </Grid.Column>
            <Grid.Column className={styles.right}>
              <span>Итого: </span>
              <span className={styles.total}>6 982</span>
              <span> exp.</span>
            </Grid.Column>
          </Grid>
          <Divider />
          <div className={styles.tableWrap} >
            <Table sortable padded='very' className={styles.table}>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell
                    sorted={column === COL_NAME ? direction : true}
                    onClick={this.handleSortName}
                  >
                    <span>Квест</span>
                  </Table.HeaderCell>
                  <Table.HeaderCell
                    sorted={column === COL_DATE ? direction : null}
                    onClick={this.handleSortDate}
                  >
                    <span>Дата</span>
                  </Table.HeaderCell>
                  <Table.HeaderCell
                    sorted={column === COL_EXP ? direction : null}
                    onClick={this.handleSortExp}
                  >
                    <span>Опыт</span>
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                { data.map(item => (
                  <Table.Row key={item.id}>
                    <Table.Cell><a>{ item.name }</a></Table.Cell>
                    <Table.Cell>{ item.created }</Table.Cell>
                    <Table.Cell><p><span className={styles.exp}>{ item.experience }</span><span> {'exp.'}</span></p></Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          </div>
        </Segment>
      </div>
    )
  }
  handleSort(clickedColumn) {
    const { column, data, direction } = this.state

    if (column !== clickedColumn) {
      return this.setState({
        column: clickedColumn,
        data: _.sortBy(data, [clickedColumn]),
        direction: DIR_ASC
      })
    }

    return this.setState({
      data: data.reverse(),
      direction: direction === DIR_ASC ? DIR_DES : DIR_ASC
    })
  }
}
