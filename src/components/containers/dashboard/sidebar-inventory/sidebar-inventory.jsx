import { connect } from 'react-redux'
import {
  Menu, Header, Divider
} from 'semantic-ui-react'

import { Sidebar as SidebarContainer } from 'components/containers'
import { redirect } from 'helpers/routing'
import {
  USER_PROFILE, USER_SKILLS, USER_GOLD, USER_EXPERIENCE, USER_LEVEL
} from 'constants/routes'

import styles from './sidebar-inventory.module.scss'

const privateItems = {
  label: 'Личные',
  items: [{
    label: 'Профиль',
    route: USER_PROFILE,
    handleClick: () => redirect(USER_PROFILE)
  }, {
    label: 'Навыки',
    route: USER_SKILLS,
    handleClick: () => redirect(USER_SKILLS)
  }, {
    label: 'Уровень',
    route: USER_LEVEL,
    handleClick: () => redirect(USER_LEVEL)
  }, {
    label: 'Опыт',
    route: USER_EXPERIENCE,
    handleClick: () => redirect(USER_EXPERIENCE)
  }, {
    label: 'Золото',
    route: USER_GOLD,
    handleClick: () => redirect(USER_GOLD)
  }]
}
const teamItems = {
  label: 'Команда',
  items: [{
    label: 'Профиль'
  }, {
    label: 'Состав'
  }, {
    label: 'Навыки'
  }, {
    label: 'Уровень'
  }, {
    label: 'Опыт'
  }, {
    label: 'Золото'
  }]
}

const SidebarInventory = props => (
  <SidebarContainer>
    <Menu vertical className={styles.menu}>
      <Header as='h2' className='content-offset page-title'>
        <Header.Content>
          Инвентарь
        </Header.Content>
      </Header>

      <Divider horizontal className={styles['sidebar-section-divider']}>{ privateItems.label }</Divider>
      <div className={styles.items}>
        { privateItems.items.map(item => (
          <Menu.Item
            key={item.label}
            link
            active={item.route === props.location}
            className={styles.item}
            onClick={item.handleClick}
          >
            { item.label }
          </Menu.Item>
        ))}
      </div>

      <Divider horizontal className={styles['sidebar-section-divider']}>{ teamItems.label }</Divider>
      <div className={styles.items}>
        { teamItems.items.map(item => (
          <Menu.Item key={item.label} link className={styles.item} onClick={item.handleClick}>
            { item.label }
          </Menu.Item>
        ))}
      </div>
    </Menu>
  </SidebarContainer>
)

SidebarInventory.propTypes = {
  location: PropTypes.string.isRequired
}

export default connect(
  state => ({
    location: state.routing.locationBeforeTransitions.pathname
  })
)(SidebarInventory)
