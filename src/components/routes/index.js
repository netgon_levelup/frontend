export { default as Login } from './login/login'
export { default as Registration } from './registration/registration'

export { default as Quests } from './quests/quests'
export { default as QuestsNew } from './quests-new/quests-new'
export { default as QuestsDetails } from './quests-details/quests-details'

export { default as UserProfile } from './user-profile/user-profile'
export { default as UserSkills } from './user-skills/user-skills'
export { default as UserLevel } from './user-level/user-level'
export { default as UserExperience } from './user-experience/user-experience'
export { default as UserGold } from './user-gold/user-gold'
