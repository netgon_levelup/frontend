import axios from 'helpers/axios'
// import { getToken } from './'

export const getAll = () => async () => {
  const result = await axios.get('categories')

  return result.data
}

export const filterByName = str => async () => {
  const result = await axios.get(`categories?filter={"where":{"name":{"regexp":"/.?${str}.*/i"}}}`)

  return result.data
}
