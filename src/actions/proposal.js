import * as c from 'constants/actions'

export function requestAdd(text) {
  return {
    type: c.API_ADD_PROPOSAL_REQUEST,
    payload: text
  }
}
export function successAdd(data) {
  return {
    type: c.API_ADD_PROPOSAL_SUCCESS,
    payload: data
  }
}
export function errorAdd(error) {
  return {
    type: c.API_ADD_PROPOSAL_ERROR,
    payload: error
  }
}

export function requestGetList(text) {
  return {
    type: c.API_GET_PROPOSALS_REQUEST,
    payload: text
  }
}
export function successGetList(data) {
  return {
    type: c.API_GET_PROPOSALS_SUCCESS,
    payload: data
  }
}
export function errorGetList(error) {
  return {
    type: c.API_GET_PROPOSALS_ERROR,
    payload: error
  }
}
