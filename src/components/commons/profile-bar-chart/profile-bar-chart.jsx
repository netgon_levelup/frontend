import { Bar as Chart } from 'react-chartjs'

import styles from './profile-bar-chart.module.scss'

const options = {
  tooltips: {
    mode: 'label',
    custom: (tooltip) => {
      /* eslint-disable no-param-reassign */
      tooltip.label = null
      tooltip.afterLabel = null
      tooltip.displayColors = false
    },
    callbacks: {
      title: item => `${item[0].yLabel} exp.`,
      label: () => '',
      afterLabel: () => ''
    }
  },
  elements: {
    rectangle: {
      borderWidth: 2,
      borderSkipped: 'bottom'
    }
  },
  responsive: false,
  legend: {
    position: 'top',
    display: false
  },
  title: {
    display: false
  }
}
const data = {
  labels: [
    10, 11, 12, 13, 14, 15, 14, 17, 18, 19, 20
  ],
  datasets: [{
    label: 'Dataset 1',
    backgroundColor: '#8a90ff',
    data: [10, 20, 30, 40, 50, 55, 50, 60, 30]
  }]
}

export default class ProfileBarChart extends React.PureComponent {
  render() {
    return (
      <Chart
        ref={(ref) => { this.chart = ref }}
        className={styles.chart}
        data={data}
        options={options}
      />
    )
  }
}
