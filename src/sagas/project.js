import {
  call,
  put,
  select,
  takeEvery
} from 'redux-saga/effects'

import { redirect } from 'helpers/routing'
import * as c from 'constants/actions'
import * as a from 'actions/project'
import { QUESTS } from 'constants/routes'
import { getId as getUserId } from './selectors/user'
import * as api from './api/project'
import * as apiStorage from './api/storage'

function* handleAdd({ payload }) {
  try {
    const params = { ...payload }
    params.userId = yield select(getUserId)

    params.files = yield params.files.map(function* (item) {
      const form = new FormData()
      form.append('file', item, item.name)
      const pfile = yield call(apiStorage.uploadProjectFile(form))

      return pfile.result.files.file[0].name
    })

    const result = yield call(api.add(params))

    if ('error' in result) {
      if (result.error.constraint === 'project_name_idx') {
        yield put(a.errorAdd(`Поле "name" должно быть уникальным.`))
      }
    } else {
      yield put(a.successAdd())
      redirect(QUESTS)
    }
  } catch (error) {
    yield put(a.errorAdd(true))
  }
}


function* handleGet(action) {
  try {
    const project = yield call(api.get(action.payload))
    yield put(a.successGet(project))
  } catch (error) {
    yield put(a.errorGet(true))
  }
}

function* handleGetList({ payload }) {
  try {
    yield put(a.successGetList({
      list: yield call(api.getList(payload)),
      count: (yield call(api.getCount())).count
    }))
  } catch (error) {
    console.log(error)
    yield put(a.errorGetList(true))
  }
}

function handleSelect(action) {
  redirect(`${QUESTS}/${action.payload.id}`)
}


export default function* watcherUser() {
  yield takeEvery(c.SELECT_PROJECT_DETAILS, handleSelect)
  yield takeEvery(c.API_ADD_PROJECT_REQUEST, handleAdd)
  yield takeEvery(c.API_GET_PROJECT_REQUEST, handleGet)
  yield takeEvery(c.API_GET_PROJECTS_REQUEST, handleGetList)
}
